//Creation of websocket server 
var WebSocketServer = require("ws").Server,
    wss = new WebSocketServer({ port: 8283 });
var uuid = require('node-uuid'),
    _ = require('lodash')._;

//list contains friends name and sallary
var friends_sallary = {
    "BAPI": 95.0,
    "MANOJ": 50.0,
    "VARUN": 300.0,
    "UMAR": 550.0,
    "ANUJ": 35.0,
    "DEEPAK": 75.0
};


// random function call to generate random no between min -max
function randomInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

var sallaryUpdater;


//***************************** Mocking a real time webservice **************************** ********************************/
// which internally calling itself and keep on update the stored values so that when client req for this, he can get new values

// method to keep on calling after random interval of time; which will modify the sallary values
var randomSallaryUpdater = function () {
    for (var Friend_name in friends_sallary) {

        // If the person is there in the array, update the sallary by adding some random value 
        if (friends_sallary.hasOwnProperty(Friend_name)) {
            var randomizedChange = randomInterval(-150, 150);
            var floatChange = randomizedChange / 100;
            friends_sallary[Friend_name] += floatChange;

        }
    }

    var randomMSTime = randomInterval(500, 2500);
    //Recursive call in random interval of time
    sallaryUpdater = setTimeout(function () {
        randomSallaryUpdater();
    }, randomMSTime);

};

//Actual call of method
randomSallaryUpdater();

// websocket open call
wss.on('connection', function (ws) {
    var client_uuid = uuid.v4();
    console.log('client [%s] connected', client_uuid);

    var clientSallaryUpdater;


    //************************************** Auto client request mocked ************************************************ */
    // By default client keep on try to send the data, but real call will hapen once client get connected with server, then 
    // clientSallarys values will get updated and it will really enter in the loop and fetch out the updated data 


    // Send the sallary updates to client for which requested
    var sendSallaryUpdates = function (ws) {
        if (ws.readyState == 1) {
            var sallaryObj = {};

            // update the sallaries fro original data array to a temp array for send
            for (var i = 0; i < clientSallarys.length; i++) {
                name = clientSallarys[i];
                sallaryObj[name] = friends_sallary[name];
            }

            // once the list updated send to client
            ws.send(JSON.stringify(sallaryObj));
        }
    };

    clientSallaryUpdater = setInterval(function () {
        sendSallaryUpdates(ws);
    }, 1000);

    var clientSallarys = [];

    // when client sent request, fetch out the request data store it in clientSallarys
    ws.on('message', function (message) {
        var sallary_request = JSON.parse(message);
        clientSallarys = sallary_request['sallary'];
        sendSallaryUpdates(ws);
    });

    ws.on('close', function () {
        if (typeof clientSallaryUpdater !== 'undefined') {
            clearInterval(clientSallaryUpdater);
        }
    });
});

console.log("started. Enojy!");



